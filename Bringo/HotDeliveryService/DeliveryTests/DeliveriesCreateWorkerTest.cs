﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using DeliveryLib;

namespace DeliveryTests
{
    [TestClass]
    public class DeliveriesCreateWorkerTest
    {
        const int EXPIRATON_TIME = 10000;

        [TestMethod]
        public void CreateDeliveriesTest()
        {
            DeliveriesControllerWithCache source = new DeliveriesControllerWithCache();

            DeliveriesCreateWorker createWorker = new DeliveriesCreateWorker(source, 10, 20, EXPIRATON_TIME);

            createWorker.CreateDeliveries();

            var sourceDeliveries = source.GetAvailableDeliveries();

            createWorker.CreateDeliveries();

            Assert.IsTrue(sourceDeliveries.Count > 0);
            Assert.IsTrue(sourceDeliveries.All(deliv => deliv.Status == DeliveryStatus.Available));
            Assert.IsTrue(sourceDeliveries.All(deliv => deliv.ExpirationTime > deliv.CreationTime));
            var unicDeliveries = sourceDeliveries.Distinct().ToList();
            Assert.IsTrue(unicDeliveries.Count == sourceDeliveries.Count);
        }
    }
}
