﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using DeliveryLib;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Data.SQLite;
using System.Data;

namespace DeliveryTests
{
    [TestClass]
    public class SaveTest
    {
        [TestMethod]
        public void SaveXmlTest()
        {
            string fileName = "testDeliveryXmlSaveFile.xml";

            File.Delete(fileName);

            List<Delivery> deliveries = new List<Delivery>();
            Delivery pizza = new Delivery(23434532, "Pizza");
            Delivery sushi = new Delivery(13336531, "Sushi");
            Delivery noodles = new Delivery(19396939, "Noodles");

            deliveries.Add(pizza);
            deliveries.Add(sushi);
            deliveries.Add(noodles);

            XmlSaver saver = new XmlSaver(fileName);

            saver.Save(deliveries);

            Assert.IsTrue(File.Exists(fileName));

            FileStream fs = new FileStream(fileName, FileMode.Open);
            XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            DataContractSerializer ser = new DataContractSerializer(typeof(List<Delivery>));
            List<Delivery> deserialisedDeliveries = (List<Delivery>)ser.ReadObject(reader, true);
            reader.Close();
            fs.Close();

            Assert.IsTrue(deliveries.Count == deserialisedDeliveries.Count);
            Assert.IsTrue(deserialisedDeliveries.Any(deliv => deliv.Id == pizza.Id && deliv.Title == pizza.Title));
            Assert.IsTrue(deserialisedDeliveries.Any(deliv => deliv.Id == sushi.Id && deliv.Title == sushi.Title));
            Assert.IsTrue(deserialisedDeliveries.Any(deliv => deliv.Id == noodles.Id && deliv.Title == noodles.Title));
        }

        [TestMethod]
        public void SaveSqlLiteTest()
        {
            string fileBaseName = "testDeliverySavebd.sqlite";
            string tableName = "deliveries";

            //delete table
            string dropSqlScript = String.Format("DROP TABLE IF EXISTS \"{0}\"", tableName);
            using (SQLiteConnection connection = new SQLiteConnection(String.Format("Data Source={0};Version=3;", fileBaseName)))
            {
                connection.Open();
                SQLiteCommand cmd = connection.CreateCommand();
                cmd.CommandText = dropSqlScript;
                cmd.ExecuteNonQuery();
            }
          
            List<Delivery> deliveries = new List<Delivery>();
            Delivery pizza = new Delivery(23434532, "Pizza");
            Delivery sushi = new Delivery(13336531, "Sushi");
            Delivery noodles = new Delivery(19396939, "Noodles");

            deliveries.Add(pizza);
            deliveries.Add(sushi);
            deliveries.Add(noodles);

            SqlLiteSaver saver = new SqlLiteSaver(fileBaseName);

            saver.Save(deliveries);

            List<Delivery> loadedFromDbDeliveries = new List<Delivery>();

            //select from table
            using (SQLiteConnection connection = new SQLiteConnection(String.Format("Data Source={0};Version=3;DateTimeKind=Utc", fileBaseName)))
            {
                connection.Open();
                SQLiteCommand cmd = connection.CreateCommand();
                cmd.CommandText = String.Format("SELECT * FROM {0};", tableName);

                SQLiteDataReader r = cmd.ExecuteReader();

                while(r.Read())
                {
                    int id = Convert.ToInt32(r["id"]);

                    string title = (string)r["title"];

                    DateTime expirationTime = (DateTime)r["expirationTime"];

                    DeliveryStatus status = (DeliveryStatus)Convert.ToInt32(r["status"]);

                    int? userId = null;
                    if (!(r["userId"] is System.DBNull))
                        userId = Convert.ToInt32(r["userId"]);

                    DateTime creationTime = (DateTime)r["creationTime"];

                    DateTime modificationTime =(DateTime)r["modificationTime"];

                    Delivery readDelivery = new Delivery(id, title, expirationTime, status, userId, creationTime, modificationTime);
                    loadedFromDbDeliveries.Add(readDelivery);
                }
                r.Close();
            }

            Assert.IsTrue(deliveries.Count == loadedFromDbDeliveries.Count);
            Assert.IsTrue(loadedFromDbDeliveries.Any(deliv => deliv.Id == pizza.Id && deliv.Title == pizza.Title && DateTimesEqual(deliv.CreationTime,pizza.CreationTime)));
            Assert.IsTrue(loadedFromDbDeliveries.Any(deliv => deliv.Id == sushi.Id && deliv.Title == sushi.Title && DateTimesEqual(sushi.CreationTime, sushi.CreationTime)));
            Assert.IsTrue(loadedFromDbDeliveries.Any(deliv => deliv.Id == noodles.Id && deliv.Title == noodles.Title && DateTimesEqual(noodles.CreationTime, noodles.CreationTime)));
        }

        bool DateTimesEqual(DateTime firstDt, DateTime secondDt)
        {
            return firstDt.Date == secondDt.Date &&
                firstDt.Hour == secondDt.Hour &&
                firstDt.Minute == secondDt.Minute &&
                firstDt.Second == secondDt.Second &&
                firstDt.Kind == secondDt.Kind;
        }
    }
}
