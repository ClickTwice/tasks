﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using DeliveryLib;
using System.Collections.Generic;
using System.Threading;

namespace DeliveryTests
{
    [TestClass]
    public class ExpiredControlWorkerTest
    {
        const int EXPIRED_TIME = 100;
        const int SLEEP_EXPIRED_INTERVAL = 150;

        [TestMethod]
        public void CheckForExpiredTest()
        {
            List<Delivery> deliveries = new List<Delivery>();
            Delivery pizza = new Delivery(23434532, "Pizza", DateTime.UtcNow.AddMilliseconds(EXPIRED_TIME));
            Delivery sushi = new Delivery(13336531, "Sushi", DateTime.UtcNow.AddMilliseconds(EXPIRED_TIME));

            deliveries.Add(pizza);
            deliveries.Add(sushi);

            DeliveriesController source = new DeliveriesController();
            ExpiredControlWorker expiredWorker = new ExpiredControlWorker(source);

            source.AddDeliveries(deliveries);

            Thread.Sleep(SLEEP_EXPIRED_INTERVAL);

            Delivery noodles = new Delivery(19396939, "Noodles", DateTime.UtcNow.AddMilliseconds(EXPIRED_TIME));
            source.AddDeliveries(new List<Delivery> { noodles });

            expiredWorker.CheckForExpired();

            Assert.IsTrue(pizza.Status == DeliveryStatus.Expired);
            Assert.IsTrue(sushi.Status == DeliveryStatus.Expired);
            Assert.IsFalse(noodles.Status == DeliveryStatus.Expired);
        }
    }
}
