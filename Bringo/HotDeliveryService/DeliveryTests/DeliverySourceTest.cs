﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeliveryLib;
using System.Collections.Generic;
using System.Linq;

namespace DeliveryTests
{
    [TestClass]
    public class DeliverySourceTest
    {
        [TestMethod]
        public void TakeDeliveryTest()
        {
            //var deliveries = CreateDeliveries();
            List<Delivery> deliveries = new List<Delivery>();
            Delivery pizza = new Delivery(23434532, "Pizza");
            Delivery sushi = new Delivery(13336531, "Sushi");
            Delivery noodles = new Delivery(19396939, "Noodles");

            deliveries.Add(pizza);
            deliveries.Add(sushi);
            deliveries.Add(noodles);

            DeliveriesController source = new DeliveriesController();

            source.AddDeliveries(deliveries);

            int idUserPizza = 234652154;
            var result = source.TakeDelivery(idUserPizza, pizza.Id);

            Assert.IsTrue(pizza.Status == DeliveryStatus.Taken);
            Assert.IsTrue(pizza.DeliveryMenId == idUserPizza);
            Assert.IsTrue(sushi.Status == DeliveryStatus.Available);
            Assert.IsTrue(sushi.DeliveryMenId == null);
            Assert.IsTrue(result.Status == TakeStatus.Taken);
        }

        [TestMethod]
        public void GetAvailableDeliveriesTest()
        {
            List<Delivery> deliveries = new List<Delivery>();
            Delivery pizza = new Delivery(23434532, "Pizza");
            Delivery sushi = new Delivery(13336531, "Sushi");
            Delivery noodles = new Delivery(19396939, "Noodles");

            deliveries.Add(pizza);
            deliveries.Add(sushi);
            deliveries.Add(noodles);

            DeliveriesController source = new DeliveriesController();

            source.AddDeliveries(deliveries);

            int idUserPizza = 234652154;
            pizza.TakeDelivery(idUserPizza);


            sushi.ChangeStatus(DeliveryStatus.Expired);

            var avaliableDelivveries = source.GetAvailableDeliveries();
            var noodlesMastBe = avaliableDelivveries.FirstOrDefault();
            Assert.IsNotNull(noodlesMastBe);
            Assert.IsTrue(avaliableDelivveries.Count == 1);
            Assert.AreEqual(noodles, noodlesMastBe);
            
        }

        List<Delivery> CreateDeliveries()
        {
            List<Delivery> deliveries = new List<Delivery>();
            deliveries.Add(new Delivery(23434532, "Pizza"));
            deliveries.Add(new Delivery(13336531, "Sushi"));
            deliveries.Add(new Delivery(19396939, "Noodles"));
            return deliveries;
        }
    }
}
