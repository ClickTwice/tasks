﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;

namespace DeliveryService
{
    public class DeliveryServiceFactory : ServiceHostFactory
    {
        public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
        {
            if (DeliveryService.Init())
            {

                Type t = Type.GetType(constructorString);
                return new ServiceHost(t, baseAddresses);
            }
            else
            {
                return null;
            }
               
        }

        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            return base.CreateServiceHost(serviceType, baseAddresses);
        }

    }
}