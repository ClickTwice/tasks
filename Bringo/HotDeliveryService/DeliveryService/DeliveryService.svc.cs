﻿using AutoMapper;
using DeliveryLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DeliveryService
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public class DeliveryService : IDeliveryService
    {
        static DeliveriesControllerWithCache source;
        static DeliveriesCreateWorker createWorker;
        static ExpiredControlWorkerWithwriteLogic expiredWorker;
        const int EXPIRED_INTERVAL = 6000;

        static IMapper dtoMapper;

        public static bool Init()
        {

            source = new DeliveriesControllerWithCache();
            //createWorker = DeliveriesCreateWorker.CreateFromConfig(source);
            //if (createWorker == null)
            //    return false;
            expiredWorker = new ExpiredControlWorkerWithwriteLogic(source, EXPIRED_INTERVAL);

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<DeliveryMen, DTObjects.DeliveryMen>();
                cfg.CreateMap<Delivery, DTObjects.Delivery>();
            });
            config.AssertConfigurationIsValid();

            dtoMapper = config.CreateMapper();

            return true;
        }

        public DTObjects.Delivery[] GetAvailableDeliveries()
        {
            try
            {
                var deliveries =  source.GetAvailableDeliveries().ToArray();
                var dtoDeliveries = dtoMapper.Map<Delivery[], DTObjects.Delivery[]>(deliveries);
                return dtoDeliveries;
            }         
            catch(Exception ex)
            {
                throw new WebFaultException<string>("Internal error", HttpStatusCode.InternalServerError);
            }
        }

        public bool TakeDelivery(int deliveryMenId, int deliveryId)
        {
            TakeResult result = new TakeResult(TakeStatus.Unknown);
            try
            {
                result = source.TakeDelivery(deliveryMenId, deliveryId);
            }
            catch(Exception ex)
            {
                throw new WebFaultException<string>("Internal error", HttpStatusCode.InternalServerError);
            }

            if (result.Status == TakeStatus.Taken)
            {
                return true;
            }
            else if (result.Status == TakeStatus.NotFound)
            {
                throw new WebFaultException<string>("Delivery not found", HttpStatusCode.NotFound);
            }
            else if (result.Status == TakeStatus.NotAvailable)
            {
                //412 нет в HttpStatusCode
                throw new WebFaultException<string>("Delivery not available", HttpStatusCode.Gone);
            }
            else if(result.Status == TakeStatus.NotTaken)
            {
                throw new WebFaultException<string>("Delivery can't be taken", HttpStatusCode.Conflict);
            }
            else
            {
                throw new WebFaultException<string>("Internal error", HttpStatusCode.InternalServerError);
            }
        }

        public DTObjects.DeliveryMen[] GetDeliveryMens()
        {
            try
            {
                //InitializeMapperIfNotInitialized();
                var deliveryMens = source.GetDeliveryMens().ToArray();
                var dtoDeliveryMens = dtoMapper.Map<DeliveryMen[], DTObjects.DeliveryMen[]>(deliveryMens);
                return dtoDeliveryMens;
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>("Internal error", HttpStatusCode.InternalServerError);
            }
        }
    }
}
