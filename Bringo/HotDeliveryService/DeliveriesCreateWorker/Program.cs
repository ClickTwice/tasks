﻿using DeliveryLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveriesCreateWorkerNameSpace
{
    class Program
    {
        static void Main(string[] args)
        {
            var source = new DeliveriesControllerWithCache();
            var createWorker = DeliveriesCreateWorker.CreateFromConfig(source);
            if (createWorker == null)
                return;

            while(true)
            {
                Console.WriteLine("DeliveriesCreateWorker Work!");
                Thread.Sleep(5000);
            }
        }
    }
}
