﻿using DeliveryLib.Savers;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryLib
{
    public class ExpiredControlWorkerWithwriteLogic
    {
        DeliveriesControllerWithCache _source;

        Timer checkTimer;
        int START_TIMER_INTERVAL = 5000;
        int _interval;

        static Logger log = LogManager.GetLogger("ExpiredControlWorker");

        public ExpiredControlWorkerWithwriteLogic(DeliveriesControllerWithCache source)
        {
            _source = source;
        }

        public ExpiredControlWorkerWithwriteLogic(DeliveriesControllerWithCache source, int interval) : this(source)
        {
            _interval = interval;
            checkTimer = new Timer(o => CheckForExpired(), null, START_TIMER_INTERVAL, _interval);
        }

        public void CheckForExpired()
        {
            log.Info("CheckForExpired...");

            DateTime curTime = DateTime.UtcNow;

            var availableExpiredDeliveries = _source.GetAvailableDeliveries()
                .Where(deliv => deliv.ExpirationTime < curTime)
                
                //необходимо сделать копию и еще раз перепроверить условия,
                //чтобы объекты не поменлись в другом потоке

                .Select(deliv => new Delivery(deliv))
                .Where(deliv => deliv.Status == DeliveryStatus.Available && deliv.ExpirationTime < curTime)
                .ToList();

            using (DeliveryDbContext writeContext = new DeliveryDbContext())
            {               
                foreach (var delivery in availableExpiredDeliveries)
                {              
                    writeContext.Deliveries.Attach(delivery);
                    delivery.ChangeStatus(DeliveryStatus.Expired);              
                }
                if (EFUtils.SaveWithExceptionHandlinDBWin(writeContext))
                    log.Info("CheckForExpired: save results ok");
            }

            log.Info("CheckForExpired completed.");
        }
    }
}
