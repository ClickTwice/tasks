﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib
{
    public class TakeResult
    {
        public TakeStatus Status { get; private set; }

        public TakeResult(TakeStatus status)
        {
            Status = status;
        }
    }
}
