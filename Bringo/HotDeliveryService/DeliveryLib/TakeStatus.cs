﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib
{
    public enum TakeStatus
    {
        Unknown = 0,
        NotFound = 1,
        NotAvailable = 2,
        Taken = 3,
        NotTaken = 4
    }
}
