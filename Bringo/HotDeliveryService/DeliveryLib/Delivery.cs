﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib
{
    public class Delivery
    {
        [Key]
        public int Id { get; set; }

        //[Column(IsVertion = true)]
        [ConcurrencyCheck]
        public DeliveryStatus Status { get; set; }

        public string Title { get; set; }

        [ConcurrencyCheck]
        public int? DeliveryMenId { get; set; }

        public DateTime CreationTime { get; set;}

        public DateTime ModificationTime {get; set;}

        public DateTime ExpirationTime { get; set; }

        public void ChangeStatus(DeliveryStatus status)
        {
            lock(this)
            {
                Status = status;
                ModificationTime = DateTime.UtcNow;
            }
        }

        public void TakeDelivery(int userId)
        {
            Status = DeliveryStatus.Taken;
            DeliveryMenId = userId;
            ModificationTime = DateTime.UtcNow;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public override bool Equals(object obj)
        {
            Delivery deliveryObj = obj as Delivery;
            if (deliveryObj != null)
                return false;
            return Id == deliveryObj.Id;
        }

        public Delivery(int id, string title)
        {
            Id = id;
            CreationTime = DateTime.UtcNow;
            Title = title;
            Status = DeliveryStatus.Available;
            ModificationTime = DateTime.UtcNow;
        }

        public Delivery(int id, string title, DateTime expirationTime) : this(id, title)
        {
            ExpirationTime = expirationTime;
        }

        public Delivery(int id, string title, DateTime expirationTime, DeliveryStatus status, int? userId,
            DateTime creationTime, DateTime modificationTime) : this(id, title, expirationTime)
        {
            Status = status;
            DeliveryMenId = userId;
            CreationTime = creationTime;
            ModificationTime = modificationTime;
        }
        
        //required to load Deliveries from base with EF
        public Delivery() { }

        public Delivery(Delivery delivery)
        {
            Id = delivery.Id;
            Title = delivery.Title;
            ExpirationTime = delivery.ExpirationTime;
            Status = delivery.Status;
            DeliveryMenId = delivery.DeliveryMenId;
            CreationTime = delivery.CreationTime;
            ModificationTime = delivery.ModificationTime;
        }
    }
}
