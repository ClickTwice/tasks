﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryLib
{
    public class DeliveriesCreateWorker
    {
        DeliveriesControllerWithCache _source;

        static Random random = new Random();

        int _minCreationCount;

        int _maxCreationCount;

        const int LENGTH_OF_TITLE = 10;

        int _expirationInMilliseconds;

        int _minInterval;
        int _maxInterval;
        Timer createTimer;

        static Logger log = LogManager.GetLogger("DeliveriesCreateWorker");

        public DeliveriesCreateWorker(DeliveriesControllerWithCache source, int minCreationCount, int maxCreationCount, int expirationInMilliseconds)
        {
            _source = source; 
            _minCreationCount = minCreationCount;
            _maxCreationCount = maxCreationCount;
            _expirationInMilliseconds = expirationInMilliseconds;
        }

        public DeliveriesCreateWorker(DeliveriesControllerWithCache source, int minCreationCount, int maxCreationCount,
            int expirationInMilliseconds, int minInrerval, int maxInterval) : 
            this(source, minCreationCount, maxCreationCount, expirationInMilliseconds)
        {
            _minInterval = minInrerval;
            _maxInterval = maxInterval;
            int initCreateInterval = GetRandomFromInterval(random, _minInterval, _maxInterval);
            createTimer = new Timer(o => 
                {
                    CreateDeliveries();
                    int newCreateInterval = GetRandomFromInterval(random, _minInterval, _maxInterval);
                    createTimer.Change(newCreateInterval, newCreateInterval);
                }              
                ,null, initCreateInterval, initCreateInterval);
        }

        public static DeliveriesCreateWorker CreateFromConfig(DeliveriesControllerWithCache source)
        {
            try
            {
                int minCreateCount = int.Parse(ConfigurationManager.AppSettings.Get("MinCreateCount"));
                int maxCreateCount = int.Parse(ConfigurationManager.AppSettings.Get("MaxCreateCount"));
                int minCreateInterval = int.Parse(ConfigurationManager.AppSettings.Get("MinCreateInterval"));
                int maxCreateInterval = int.Parse(ConfigurationManager.AppSettings.Get("MaxCreateInterval"));
                int expiratonTime = int.Parse(ConfigurationManager.AppSettings.Get("ExpirationTime"));
                log.Info("CreateFromConfig ok");
                return new DeliveriesCreateWorker(source, minCreateCount, maxCreateCount, 
                    expiratonTime, minCreateInterval, maxCreateInterval);
            }
            catch(Exception ex)
            {
                log.Error(ex, "CreateFromConfig error");
                return null;
            }
        }

        public void CreateDeliveries()
        {
            int countOfDelivery = GetRandomFromInterval(random, _minCreationCount, _maxCreationCount);
            var newDeliveries = CreateDeliveries(countOfDelivery);
            _source.AddNewDeliveries(newDeliveries);
            log.Info(String.Format("Created {0} deliveries", countOfDelivery));
        }

        List<Delivery> CreateDeliveries(int count)
        {
            List<Delivery> deliveries = new List<Delivery>();
            while(count > 0)
            {
                string title = String.Empty;
                lock(random)
                {
                    title = GetTitle();
                }
                //title, DateTime.UtcNow.AddMilliseconds(_expirationInMilliseconds)

                Delivery delivery = new Delivery
                {
                    Title = title,
                    CreationTime = DateTime.UtcNow,
                    ModificationTime = DateTime.UtcNow,
                    ExpirationTime = DateTime.UtcNow.AddMilliseconds(_expirationInMilliseconds),
                    Status = DeliveryStatus.Available

                };
                deliveries.Add(delivery);
                count--;
            }
            return deliveries;
        }

        static string GetTitle()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, LENGTH_OF_TITLE)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        static int GetRandomFromInterval(Random random, int min, int max)
        {
            int result = 0;
            //ранодом не потокобезопасный
            lock (random)
            {
                result = random.Next(min, max);
            }
            return result;
        }
    }
}
