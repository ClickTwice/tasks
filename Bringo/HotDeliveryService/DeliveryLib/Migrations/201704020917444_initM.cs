namespace DeliveryLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initM : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Title = c.String(unicode: false),
                        UserId = c.Int(),
                        CreationTime = c.DateTime(nullable: false, precision: 0),
                        ModificationTime = c.DateTime(nullable: false, precision: 0),
                        ExpirationTime = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Deliveries");
        }
    }
}
