namespace DeliveryLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeliveryMen : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeliveryMen",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Location = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);

            RenameColumn("dbo.Deliveries", "UserId", "DeliveryMenId");
            CreateIndex("dbo.Deliveries", "DeliveryMenId");
            AddForeignKey("dbo.Deliveries", "DeliveryMenId", "dbo.DeliveryMen", "Id");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Deliveries", "DeliveryMenId", "dbo.DeliveryMen");
            DropIndex("dbo.Deliveries", new[] { "DeliveryMenId" });
            RenameColumn("dbo.Deliveries", "DeliveryMenId", "UserId");

            DropTable("dbo.DeliveryMen");

            
        }
    }
}
