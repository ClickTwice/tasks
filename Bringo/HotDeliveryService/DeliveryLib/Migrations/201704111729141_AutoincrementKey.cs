namespace DeliveryLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AutoincrementKey : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Deliveries", "Id", c => c.Int(nullable: false, identity: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Deliveries", "Id", c => c.Int(nullable: false));
        }
    }
}
