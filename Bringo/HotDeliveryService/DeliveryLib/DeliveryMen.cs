﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib
{
    public class DeliveryMen
    {
        [Key]
        public int Id { get; set; }

        [ConcurrencyCheck]
        public string Name { get; set; }

        [ConcurrencyCheck]
        public string Location { get; set; }

        [ConcurrencyCheck]
        public virtual List<Delivery> Deliveries { get; set; }

        public DeliveryMen() { }
    }
}
