﻿using DeliveryLib.Savers;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryLib
{
    public class DeliveriesController
    {
        static Logger log = LogManager.GetLogger("DeliveriesController");

        IDataSource dataSource;

        public DeliveriesController()
        {
            dataSource = DeliverySourceFactory.GetDataSource();          
        }

        public List<Delivery> GetAvailableDeliveries()
        {
            return dataSource.GetDeliveries().Where(deliv => deliv.Status == DeliveryStatus.Available).ToList();
        }

        public void AddDeliveries(List<Delivery> deliveries)
        {
            log.Info("Added {0} deliveries", deliveries.Count);
            dataSource.AddDeliveries(deliveries);
        }

        public TakeResult TakeDelivery(int userId, int deliveryId)
        {
            log.Info("{0} user try take {1} delivery", userId, deliveryId);
            var foundDelivery = dataSource.GetDeliveries().FirstOrDefault(deliv => deliv.Id == deliveryId);
            if(foundDelivery == null)
            {
                log.Info("Delivery {0} not found", deliveryId);
                return new TakeResult(TakeStatus.NotFound);
            }

            var deliveryForSave = new Delivery(foundDelivery);

            if (deliveryForSave.Status == DeliveryStatus.Available)
            {
                using (DeliveryDbContext saveDiliveryContext = new DeliveryDbContext())
                {
                    //string StringLog = string.Empty;
                    //saveDiliveryContext.Database.Log += msg => StringLog = StringLog + msg + Environment.NewLine ;

                    saveDiliveryContext.Deliveries.Attach(deliveryForSave);
                    deliveryForSave.TakeDelivery(userId);
                    
                    //if (dataSource.SaveDelivery(foundDelivery))
                    if(saveDiliveryContext.SaveChanges() > 0)
                    {
                        log.Info("{0} user took {1} delivery", userId, deliveryId);
                        return new TakeResult(TakeStatus.Taken);
                    }
                    else
                    {
                        log.Info("{0} user coudn't save taken {1} delivery", userId, deliveryId);
                        return new TakeResult(TakeStatus.NotTaken);
                    }
                }                  
            }
            else
            {
                log.Info("Delivery {0} not available", userId);
                return new TakeResult(TakeStatus.NotAvailable);
            }
            
        }

        public List<Delivery> GetDeliveries()
        {
            return dataSource.GetDeliveries();
        }
    }
}
