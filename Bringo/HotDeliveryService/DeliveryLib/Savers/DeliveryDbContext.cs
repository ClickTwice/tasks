﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public class DeliveryDbContext : DbContext
    {
        public DeliveryDbContext(string connectionString, string connectionStringName)
            : base(connectionStringName)
        {
            this.Database.Connection.ConnectionString = connectionString;
        }

        public DeliveryDbContext(): base("name=Deliveries") { }

        public DeliveryDbContext(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public virtual DbSet<Delivery> Deliveries { get; set; }

        public virtual DbSet<DeliveryMen> DeliveryMens { get; set; }

    }

}
