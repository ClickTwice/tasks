﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public class XmlDataSource //: IDataSource
    {
        string _fileName;

        static Logger log = LogManager.GetCurrentClassLogger();

        public XmlDataSource(string fileName)
        {
            _fileName = fileName;
        }

        public List<Delivery> GetDeliveries()
        {
            return null;
        }


        public void AddDeliveries(List<Delivery> deliveries)
        {

        }

        public void DeleteDeliveries(List<Delivery> deliveries)
        {

        }

        void Save(List<Delivery> deliveries)
        {
            try
            {
                FileStream writer = new FileStream(Utils.GetFullPathToDb(_fileName), FileMode.Create);
                DataContractSerializer ser = new DataContractSerializer(typeof(List<Delivery>));
                ser.WriteObject(writer, deliveries);
                writer.Close();
            }
            catch (Exception ex)
            {
                log.Error(ex, "Error save to xml");
            }
        }
    }
}
