﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public class DeliverySourceFactory
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        const string SAVER_KEY = "Saver";

        const string SQL_LITE_SAVER = "SqlLiteSaver";
        const string SQL_LITE_FILE = "testDeliverySavebd.sqlite";

        const string XML_SAVER = "XmlSaver";
        const string XML_FILE = "testDeliveryXmlSaveFile.xml";

        const string EF_SAVER_SQLITE = "EFSaverSQLite";

        const string EF_SAVER_SQL = "EFSaverSQL";

        public static IDataSource GetDataSource()
        {
            string saverType = ConfigurationManager.AppSettings.Get(SAVER_KEY);
            //if (saverType == SQL_LITE_SAVER)
            //{
            //    log.Info("Create Sql Lite Saver...");
            //    return new SqlLiteDataSource(SQL_LITE_FILE);
            //}
            //else if (saverType == XML_SAVER)
            //{
            //    log.Info("Create Xml Saver...");
            //    return new XmlDataSource(XML_FILE);
            //}
            //else if (saverType == EF_SAVER_SQLITE)
            //{
            //    log.Info("Create EF Saver SQLite");
            //    return new EFSQLiteDataSource(SQL_LITE_FILE);
            //}
            if (saverType == EF_SAVER_SQL)
            {
                log.Info("Create EF Saver SQL");
                return new EFSQLDataSource();
            }
            else
            {
                log.Info("There are no saver");
                return null;
            }
        }
    }
}
