﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public interface IDataSource
    {
        List<Delivery> GetDeliveries();

        void AddDeliveries(List<Delivery> deliveries);

        void DeleteDeliveries(List<Delivery> deliveries);

        bool SaveChanges();

        bool SaveDelivery(Delivery delivery);
    }
}
