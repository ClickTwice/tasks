﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public class Utils
    {
        public static string GetFullPathToDb(string fileName)
        {
            string fullTime = Path.Combine(Environment.CurrentDirectory, fileName);
            return fullTime;
        }

        //создаем таблицу если она не существует
        public static void CreateTableIfNotExist(SQLiteConnection connection, string deliveryTableName)
        {
            SQLiteCommand cmd = connection.CreateCommand();

            StringBuilder createIfNotExist = new StringBuilder();

            //IF NOT EXISTS
            createIfNotExist.Append(String.Format("CREATE TABLE IF NOT EXISTS \"{0}\" (", deliveryTableName));

            createIfNotExist.Append(String.Format("\"id\" INTEGER PRIMARY KEY  NOT NULL,"));
            createIfNotExist.Append(String.Format("\"title\" TEXT DEFAULT \"\","));
            createIfNotExist.Append(String.Format("\"expirationTime\" DATETIME DEFAULT \"2000-01-01 00:00\","));
            createIfNotExist.Append(String.Format("\"status\" INTEGER NOT NULL,"));
            createIfNotExist.Append(String.Format("\"userId\" INTEGER DEFAULT NULL,"));
            createIfNotExist.Append(String.Format("\"creationTime\" DATETIME DEFAULT \"2000-01-01 00:00\","));
            createIfNotExist.Append(String.Format("\"modificationTime\" DATETIME DEFAULT \"2000-01-01 00:00\""));

            createIfNotExist.Append(");");

            cmd.CommandText = createIfNotExist.ToString();

            cmd.ExecuteNonQuery();
        }


    }
}
