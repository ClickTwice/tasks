﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public class ReadDeliveriesCache
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        const string CONNECTION_STRING_NAME = "name=Deliveries";

        DeliveryDbContext curContext;

        List<Delivery> deliveries;

        List<DeliveryMen> deliveryMens;

        Timer saveAndUpdateChacheTimer;
        int UPDATE_TIME_INTERVAL = 10000;

        //интервал времени в сек. в котором загруженны данные по табл Deliveries;
        int TIME_INTERVAL_FOR_DATA_SEC = 36000;

        public ReadDeliveriesCache()
        {
            curContext = new DeliveryDbContext(CONNECTION_STRING_NAME);

            deliveries = new List<Delivery>();
            deliveryMens = new List<DeliveryMen>();

            saveAndUpdateChacheTimer = new Timer(o =>
            {
                UpdateChache();
            },
                null, 0, UPDATE_TIME_INTERVAL);
        }

        public List<Delivery> GetDeliveries()
        {
            return deliveries.ToList();      
        }

        public List<DeliveryMen> GetDeliveryMens()
        {
            return deliveryMens.ToList();
        }

        void UpdateChache()
        {
            try
            {
                using (var curContext = new DeliveryDbContext(CONNECTION_STRING_NAME))
                {
                    //curContext.Configuration.ProxyCreationEnabled = false;

                    DateTime lowestDt = DateTime.UtcNow.AddSeconds(-TIME_INTERVAL_FOR_DATA_SEC);

                    string queryStr = string.Empty;
                    curContext.Database.Log += msg => queryStr = queryStr + msg + Environment.NewLine;

                    deliveries = curContext.Deliveries.Where(dl => dl.CreationTime > lowestDt).ToList();

                    //будет ли переопределенны уже загруженные Deliveries?(походу нет)
                    deliveryMens = curContext.DeliveryMens
                        .Include(men => men.Deliveries)
                        .ToList();                
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }
}
