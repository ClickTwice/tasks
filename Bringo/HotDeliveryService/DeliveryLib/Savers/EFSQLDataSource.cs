﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public class EFSQLDataSource : IDataSource
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        const string CONNECTION_STRING_NAME = "name=Deliveries";

        DeliveryDbContext curContext;

        Timer saveAndUpdateChacheTimer;
        int SAVE_AND_UPDATE_TIME_INTERVAL = 10000;
        int SAVE_AND_UPDATE_TIME_START = 10000;

        //интервал времени в сек. в котором загруженны данные по табл Deliveries;
        int TIME_INTERVAL_FOR_DATA_SEC = 36000;

        public EFSQLDataSource()
        {
            curContext = new DeliveryDbContext(CONNECTION_STRING_NAME);

            DateTime lowestDt = DateTime.UtcNow.AddSeconds(-TIME_INTERVAL_FOR_DATA_SEC);
            curContext.Deliveries.Where(dl => dl.CreationTime > lowestDt).ToList();

            saveAndUpdateChacheTimer = new Timer(o =>
            {
                SaveAndUpdateChache();
            },
                null, SAVE_AND_UPDATE_TIME_START, SAVE_AND_UPDATE_TIME_INTERVAL);
        }

        object lockObj = new object();

        public List<Delivery> GetDeliveries()
        {     
            //lock(lockObj)
            //{
                return curContext.Deliveries.Local.ToList();
            //}            
        }

        public void AddDeliveries(List<Delivery> deliveries)
        {
            //return;
            //lock(lockObj)
            //{
                curContext.Deliveries.AddRange(deliveries);
            SaveChanges();
            //}
        }

        public void DeleteDeliveries(List<Delivery> deliveries)
        {
            curContext.Deliveries.RemoveRange(deliveries);
            SaveChanges();
        }

        /// <summary>
        /// Сохраняет изменения. В случае конфликтов, изменения перезаписываются из бызы данных.
        /// </summary>
        /// <returns>Возвращает true, если сохранени прошли без проблем(не было конфликтов и исключений).
        /// В противном случае false.</returns>
        public bool SaveChanges()
        {
            bool allOk = true;
            while(true)
            {
                try
                {
                    curContext.SaveChanges();
                    return allOk;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    log.Error(ex);
                    foreach (var entity in ex.Entries)
                    {
                        entity.Reload();
                    }
                    allOk = false;
                }
                catch (Exception ex)
                {
                    allOk = false;
                    log.Error(ex);
                    return allOk;
                }
            }
        }

        /// <summary>
        /// сохранение доставкив новом контексте, 
        /// т.к. это не приводим к проблеммам параллельности
        /// </summary>
        /// <param name="delivery"></param>
        /// <returns></returns>
        public bool SaveDelivery(Delivery delivery)
        {
            using (DeliveryDbContext saveDiliveryContext = new DeliveryDbContext())
            {
                string StringLog = string.Empty;
                saveDiliveryContext.Database.Log += msg => StringLog = StringLog + msg + Environment.NewLine;


                bool allOk = true;
                while (true)
                {
                    try
                    {
                        //attach вместо добавления не должно вызвать проблем дублирования
                        saveDiliveryContext.Deliveries.Attach(delivery);
                        saveDiliveryContext.Entry(delivery).State = System.Data.Entity.EntityState.Modified;

                        saveDiliveryContext.SaveChanges();
                        return allOk;
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        log.Error(ex);
                        foreach (var entity in ex.Entries)
                        {
                            entity.Reload();
                        }
                        allOk = false;
                    }
                    catch (Exception ex)
                    {
                        allOk = false;
                        log.Error(ex);
                        return allOk;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        void SaveAndUpdateChache()
        {
            try
            {
                SaveChanges();

                DateTime lowestDt = DateTime.UtcNow.AddSeconds(-TIME_INTERVAL_FOR_DATA_SEC);

                curContext.Deliveries.Where(dl => dl.CreationTime > lowestDt).ToList();
            }
            catch(Exception ex)
            {
                log.Error(ex);
            }
        }
    }
}
