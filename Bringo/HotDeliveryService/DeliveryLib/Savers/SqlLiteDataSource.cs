﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public class SqlLiteDataSource //: IDataSource
    {

        string _fileBaseName;
        const string DELIVERIES_TABLE_NAME = "deliveries";

        static Logger log = LogManager.GetCurrentClassLogger();

        public SqlLiteDataSource(string fileBaseName)
        {
            _fileBaseName = fileBaseName;
        }

        public List<Delivery> GetDeliveries()
        {
            return null;
        }


        public void AddDeliveries(List<Delivery> deliveries)
        {

        }

        public void DeleteDeliveries(List<Delivery> deliveries)
        {

        }

        void Save(List<Delivery> deliveries)
        {
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(String.Format("Data Source={0};Version=3;DateTimeKind=Utc", Utils.GetFullPathToDb(_fileBaseName))))
                {
                    connection.Open();

                    Utils.CreateTableIfNotExist(connection, DELIVERIES_TABLE_NAME);

                    InsertOrReplaceDeliveries(connection, deliveries);
                }
            }
            catch(Exception ex)
            {
                log.Error(ex, "Error save to SQLite");
            }
        }



        //кладем новые данные и заменяем старые
        void InsertOrReplaceDeliveries(SQLiteConnection connection, List<Delivery> deliveries)
        {
            SQLiteCommand cmd = connection.CreateCommand();

            StringBuilder inserOrReplace = new StringBuilder();

            inserOrReplace.Append(String.Format("INSERT OR REPLACE into {0} " +
                "(id, title, expirationTime, status, userId, creationTime, modificationTime) VALUES", DELIVERIES_TABLE_NAME));

            foreach(var deliv in deliveries)
            {
                inserOrReplace.Append(String.Format("({0}, '{1}', '{2}', '{3}', {4}, '{5}', '{6}'),"
                    , deliv.Id, deliv.Title, ToSqlUnixTimeString(deliv.ExpirationTime), (int)deliv.Status, deliv.DeliveryMenId.HasValue ? deliv.DeliveryMenId.ToString() : "NULL",
                    ToSqlUnixTimeString(deliv.CreationTime), ToSqlUnixTimeString(deliv.ModificationTime)));
            }

            //удаляем последнюю запятую
            inserOrReplace.Remove(inserOrReplace.Length - 1, 1);

            inserOrReplace.Append(";");

            cmd.CommandText = inserOrReplace.ToString();

            cmd.ExecuteNonQuery();
        }

        string ToSqlUnixTimeString(DateTime dateTime)
        {
            string strDt = dateTime.ToString("yyyy-MM-dd HH:mm:ss") + "Z";
            return strDt;
        }

    }
}
