﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public static class EFUtils
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Пытается сохранить изменения в течении некоторого количества попыток. 
        /// В случае конфликта принимает изменения базы.
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="countOfPrompts">Максимальное кол-во попыток</param>
        /// <returns></returns>
        public static bool SaveWithExceptionHandlinDBWin(DbContext context, int countOfPrompts = 10)
        {
            bool allOk = true;
            int promptsCouter = 0;
            while (true)
            {
                try
                {
                    promptsCouter++;
                    context.SaveChanges();
                    return allOk;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    log.Error(ex);
                    foreach (var entity in ex.Entries)
                    {
                        entity.Reload();
                    }
                    allOk = false;
                    if (promptsCouter >= countOfPrompts)
                        return allOk;
                }
                catch (Exception ex)
                {
                    allOk = false;
                    log.Error(ex);
                    if (promptsCouter >= countOfPrompts)
                        return allOk;
                }
            }
        }
    }
}
