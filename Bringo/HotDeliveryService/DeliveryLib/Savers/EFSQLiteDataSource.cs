﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib.Savers
{
    public class EFSQLiteDataSource //: IDataSource
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        string connectionString;

        const string DELIVERIES_TABLE_NAME = "Deliveries";

        public EFSQLiteDataSource(string fileBaseName)
        {
            connectionString = String.Format("Data Source={0};Version=3;DateTimeKind=Utc", Utils.GetFullPathToDb(fileBaseName));
        }

        public List<Delivery> GetDeliveries()
        {
            return null;
        }


        public void AddDeliveries(List<Delivery> deliveries)
        {

        }

        public void DeleteDeliveries(List<Delivery> deliveries)
        {

        }

        void Save(List<Delivery> deliveries)
        {
            using (DeliveryDbContext dbContext = new DeliveryDbContext(connectionString, "name=EFSqlLite"))
            {
                CreateTableIfNotExist(dbContext.Database.Connection);

                dbContext.Deliveries.AddRange(deliveries);
                dbContext.SaveChanges();
            }
        }

        void CreateTableIfNotExist(DbConnection connection)
        {
            if (connection.State != System.Data.ConnectionState.Open)
                connection.Open();

            DbCommand cmd = connection.CreateCommand();

            StringBuilder createIfNotExist = new StringBuilder();

            //IF NOT EXISTS
            createIfNotExist.Append(String.Format("CREATE TABLE IF NOT EXISTS \"{0}\" (", DELIVERIES_TABLE_NAME));

            createIfNotExist.Append(String.Format("\"Id\" INTEGER PRIMARY KEY  NOT NULL,"));
            createIfNotExist.Append(String.Format("\"Title\" TEXT DEFAULT \"\","));
            createIfNotExist.Append(String.Format("\"ExpirationTime\" DATETIME DEFAULT \"2000-01-01 00:00\","));
            createIfNotExist.Append(String.Format("\"Status\" INTEGER NOT NULL,"));
            createIfNotExist.Append(String.Format("\"UserId\" INTEGER DEFAULT NULL,"));
            createIfNotExist.Append(String.Format("\"CreationTime\" DATETIME DEFAULT \"2000-01-01 00:00\","));
            createIfNotExist.Append(String.Format("\"ModificationTime\" DATETIME DEFAULT \"2000-01-01 00:00\""));

            createIfNotExist.Append(");");

            cmd.CommandText = createIfNotExist.ToString();

            cmd.ExecuteNonQuery();
        }




    }
}
