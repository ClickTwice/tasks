﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryLib
{
    public class ExpiredControlWorker
    {
        DeliveriesController _source;

        Timer checkTimer;
        int START_TIMER_INTERVAL = 5000;
        int _interval;

        static Logger log = LogManager.GetLogger("ExpiredControlWorker");

        public ExpiredControlWorker(DeliveriesController source)
        {
            _source = source; 
        }

        public ExpiredControlWorker(DeliveriesController source, int interval) : this(source)
        {
            _interval = interval;
            checkTimer = new Timer(o => CheckForExpired(), null, START_TIMER_INTERVAL, _interval);
        }

        public void CheckForExpired()
        {
            log.Info("CheckForExpired...");
            var availableDeliveries = _source.GetAvailableDeliveries();
            DateTime curTime = DateTime.UtcNow;
            foreach(var delivery in availableDeliveries)
            {
                if (delivery.ExpirationTime < curTime)
                    delivery.ChangeStatus(DeliveryStatus.Expired);
            }
            log.Info("CheckForExpired completed.");
        }
    }
}
