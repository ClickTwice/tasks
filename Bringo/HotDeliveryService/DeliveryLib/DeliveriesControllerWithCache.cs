﻿using DeliveryLib.Savers;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryLib
{
    public class DeliveriesControllerWithCache
    {
        static Logger log = LogManager.GetCurrentClassLogger();

        ReadDeliveriesCache cache;

        public DeliveriesControllerWithCache()
        {
            cache = new ReadDeliveriesCache();
        }

        public List<Delivery> GetAvailableDeliveries()
        {
            return cache.GetDeliveries().Where(deliv => deliv.Status == DeliveryStatus.Available).ToList();
        }

        public void AddNewDeliveries(List<Delivery> deliveries)
        {
            log.Info("Added {0} deliveries", deliveries.Count);
            using (DeliveryDbContext writeContext = new DeliveryDbContext())
            {
                writeContext.Deliveries.AddRange(deliveries);

                try
                {
                    writeContext.SaveChanges();
                }
                catch(Exception ex)
                {
                    log.Error(ex);
                }
            }
        }

        public TakeResult TakeDelivery(int deliveryMenId, int deliveryId)
        {
            log.Info("{0} user try take {1} delivery", deliveryMenId, deliveryId);
            var foundDelivery = cache.GetDeliveries().FirstOrDefault(deliv => deliv.Id == deliveryId);
            if (foundDelivery == null)
            {
                log.Info("Delivery {0} not found", deliveryId);
                return new TakeResult(TakeStatus.NotFound);
            }

            //необходимо т.к. found delivety может быть изменена в других местах.
            var takeDelivery = new Delivery(foundDelivery);

            if (takeDelivery.Status == DeliveryStatus.Available)
            {
                using (DeliveryDbContext writeContext = new DeliveryDbContext())
                {
                    //string StringLog = string.Empty;
                    //saveDiliveryContext.Database.Log += msg => StringLog = StringLog + msg + Environment.NewLine ;

                    writeContext.Deliveries.Attach(takeDelivery);
                    takeDelivery.TakeDelivery(deliveryMenId);

                    if (EFUtils.SaveWithExceptionHandlinDBWin(writeContext))
                    {
                        log.Info("{0} user took {1} delivery", deliveryMenId, deliveryId);
                        return new TakeResult(TakeStatus.Taken);
                    }
                    else
                    {
                        log.Info("{0} user coudn't save taken {1} delivery", deliveryMenId, deliveryId);
                        return new TakeResult(TakeStatus.NotTaken);
                    }
                }
            }
            else
            {
                log.Info("Delivery {0} not available", deliveryMenId);
                return new TakeResult(TakeStatus.NotAvailable);
            }

        }

        public List<Delivery> GetDeliveries()
        {
            return cache.GetDeliveries();
        }

        public List<DeliveryMen> GetDeliveryMens()
        {
            return cache.GetDeliveryMens();
        }
    }
}
