﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCalculator
{
    class Program
    {
        const string ADD = "+";
        const string SUB = "-";
        const string MUL = "*";
        const string DIV = "/";

        const string ZERO = "0";
        const int ARGUMENT_CURRENCY = 15;
        const string DELIMITER = ",";

        static void Main(string[] args)
        {
            try
            {
                //проверка на перепонение
                checked
                {
                    int countOfNumbersFirstArg = 0;
                    double firstArg = GetArgument(args[0], 1, ref countOfNumbersFirstArg);
                        
                    string operand = args[1];

                    int countOfNumbersSecondArg = 0;
                    double secondArg = GetArgument(args[2], 2, ref countOfNumbersSecondArg);


                    if(countOfNumbersFirstArg + countOfNumbersSecondArg > ARGUMENT_CURRENCY)
                    {
                        Console.WriteLine("Loss of precision in result maybe");
                    }

                    double result = Calculate(firstArg, operand, secondArg);
                    Console.WriteLine(result);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }        
        }

        static double GetArgument(string argumentStr, int numberOfArgument, ref int countOfNumbers)
        {
            //преобразование из string в double
            double argument = 0;
            if (!double.TryParse(argumentStr, out argument))
            {
                throw new FormatException(String.Format("Bad argument {0} format", numberOfArgument));
            }

            //проверка на точность
            countOfNumbers = argumentStr.Replace(DELIMITER, "").Count();
            if (countOfNumbers > ARGUMENT_CURRENCY)
            {
                Console.WriteLine(String.Format("Loss of precision in {0} argument", numberOfArgument));
            }
                
            return argument;
        }

        static double Calculate(double firstArg, string operand, double secondArg)
        {
            switch(operand)
            {
                case ADD:
                    return firstArg + secondArg;
                case SUB:
                    return firstArg - secondArg;
                case MUL:
                    return firstArg * secondArg;
                case DIV:
                    return firstArg / secondArg;
                default:
                    throw new ArgumentException("Unknown operand");            
            }
        }

        
    }

   
}
