﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            //expected 4
            var result = Solution.solution(new int[] { 2, -2, 3, 0, 4, -7 });
            //expected 3
            result = Solution.solution(new int[] { -2, -1, 0, 1, 2 });
        }
    }



    class Solution
    {
        public static  int solution(int[] A)
        {
            //the idea is: when we get the same sum that was calculated in previous time 
            //we've got another one zero subarray

            //sums - dictionary: key - sum, vaule - count of times we meat this sum
            Dictionary<int, int> sums = new Dictionary<int,int>();
            int lastSum = 0;
            sums[lastSum] = 1;
            foreach (var a in A)
            {
                lastSum = lastSum + a;
                sums.TryGetValue(lastSum, out int previosValue);
                sums[lastSum] = previosValue + 1;
            }

            var countOfZeroSumSubaraies = sums.Where(sumRecord => sumRecord.Value > 1)
                .Select(sumRecord => sumRecord.Value)
                //countOfSumes * (countOfSumes + 1) / 2  - count of pairs from equal sum elements
                .Sum(countOfSumes => countOfSumes * (countOfSumes - 1) / 2);

            return countOfZeroSumSubaraies;
        }
    }



}